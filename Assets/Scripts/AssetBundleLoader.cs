﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetBundleLoader : MonoBehaviour {

    public GameObject buttonPrefab;
    public GameObject wheelPanel;
    public WheelsManager wheelManager;


    // Use this for initialization
    void Start () {
        StartCoroutine(getBundles());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator getBundles()
    {
        while (!Caching.ready)
            yield return null;
        // Start a download of the given URL
        string url = "file:///" + Application.dataPath + "/AssetBundles/wheel/vossen_lc_102";

        WWW www = WWW.LoadFromCacheOrDownload(url, 1);
        // Wait for download to complete
        yield return www;
        // Load and retrieve the AssetBundle
        AssetBundle bundle = www.assetBundle;


        AssetBundleRequest request = bundle.LoadAssetAsync("Vossen_LC_102 (2).png", typeof(Sprite));
        yield return request;
        Sprite wheelImage = request.asset as Sprite;

        request = bundle.LoadAssetAsync("Front_Right_Vossen_LC_102 1.prefab", typeof(GameObject));
        yield return request;
        GameObject wheelTR = request.asset as GameObject;

        request = bundle.LoadAssetAsync("Back_Left_Vossen_LC_102.prefab", typeof(GameObject));
        yield return request;
        GameObject wheelBL = request.asset as GameObject;

        request = bundle.LoadAssetAsync("Back_Right_Vossen_LC_102.prefab", typeof(GameObject));
        yield return request;
        GameObject wheelBR = request.asset as GameObject;

        request = bundle.LoadAssetAsync("Front_Left_Vossen_LC_102.prefab", typeof(GameObject));
        yield return request;
        GameObject wheelTL = request.asset as GameObject;



        // Unload the AssetBundles compressed contents to conserve memory
        bundle.Unload(false);

        // Frees the memory from the web stream
        www.Dispose();

        GameObject button = GameObject.Instantiate(buttonPrefab, wheelPanel.transform);
        button.GetComponent<WheelButton>().SetImage(wheelImage);
        button.GetComponent<WheelButton>().SetWheels(wheelTL, wheelTR, wheelBL, wheelBR);
        button.GetComponent<WheelButton>().wheelManager = wheelManager;
    }

}
