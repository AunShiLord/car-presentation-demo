﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonMove : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public int direction = 0;
    public MoveAround moveAroundScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        moveAroundScript.direction = direction;
        moveAroundScript.speed = 40f;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        moveAroundScript.speed = moveAroundScript.defaultSpeed;
    }
}
