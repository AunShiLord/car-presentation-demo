﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonZoom : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public float speed = 0f;
    public MoveAround moveAroundScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        moveAroundScript.forwardSpeed = speed;
        /*
        Debug.Log(camera.transform.position);
        camera.transform.Translate(camera.transform.forward * speed);
        Debug.Log(camera.transform.position + "\n");
        */
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        moveAroundScript.forwardSpeed = 0;
        //moveAroundScript.speed = moveAroundScript.defaultSpeed;
    }
}
