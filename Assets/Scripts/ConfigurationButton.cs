﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationButton : MonoBehaviour {

    public ConfigurationLoader configurationLoader;
    public Text text;
    public string slotName;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void pressButton()
    {
        configurationLoader.HandleButtonPress(this, slotName, text.text);
    }

    public void SetNewText(string _text)
    {
        // fantastic line of code
        text.text = _text;
    }
}
