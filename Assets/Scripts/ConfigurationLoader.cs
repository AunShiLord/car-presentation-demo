﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationLoader : MonoBehaviour {

    public ConfigurationButton[] buttons;
    public Renderer colorExample;

    public Painter painter;
    private bool isSaveMode = true;

	// Use this for initialization
	void Start () {
        UpdateButtons();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void UpdateButtons()
    {
        List<SaveData> data = new List<SaveData>();

        string json = PlayerPrefs.GetString("slot1", "empty");
        data.Add(JsonUtility.FromJson<SaveData>(json));

        json = PlayerPrefs.GetString("slot2", "empty");
        data.Add(JsonUtility.FromJson<SaveData>(json));

        json = PlayerPrefs.GetString("slot3", "empty");
        data.Add(JsonUtility.FromJson<SaveData>(json));

        json = PlayerPrefs.GetString("slot4", "empty");
        data.Add(JsonUtility.FromJson<SaveData>(json));

        foreach (ConfigurationButton button in buttons)
        {
            foreach (SaveData saveData in data)
            {
                if (saveData.slotName == button.slotName)
                {
                    button.SetNewText(saveData.name);
                    continue;
                }
            }
        }
    }

    public void ChangeMode(bool isSave)
    {
        isSaveMode = isSave;
    }

    public void HandleButtonPress(ConfigurationButton button, string slotName, string text)
    {
        if (isSaveMode)
        {
            button.SetNewText(DateTime.Now.ToString("MM/dd/yyyy h:mm tt"));
            SaveConfiguration(slotName, button.text.text);
        }
        else
        {
            LoadConfiguration(slotName);
        }
    }

    public void LoadConfiguration(string slotName)
    {
        string json = PlayerPrefs.GetString(slotName, "empty");
        if (json != "empty")
        {
            SaveData saveData = JsonUtility.FromJson<SaveData>(json);

            painter.ChangeMaterial(saveData.materialIndex);
            Color color = new Color(saveData.r, saveData.g, saveData.b);
            painter.RePaint(color);
            painter.UpdateSliders();
        }
        
    }

    public void SaveConfiguration(string slotName, string name)
    {
        int materialIndex = 0;
        for (int i = 0; i < painter.baseMaterial.Length; i++)
        {
            if (colorExample.sharedMaterial.name.Contains(painter.baseMaterial[i].name))
            {
                materialIndex = i;
                break;
            }
        }

        SaveData saveData = new SaveData();
        saveData.name = name;
        saveData.r = colorExample.material.color.r;
        saveData.g = colorExample.material.color.g;
        saveData.b = colorExample.material.color.b;
        saveData.materialIndex = materialIndex;
        saveData.slotName = slotName;
        string json = JsonUtility.ToJson(saveData);
        PlayerPrefs.SetString(slotName, json);


    }
}
