﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

    public Animator animator;
    public GameObject colorPannel;
    public GameObject materialPanel;
    public GameObject wheelsPanel;

    private bool isMenuOpened = false;
   

    // Use this for initialization
	void Start () {
        colorPannel.SetActive(false);
        materialPanel.SetActive(false);
        wheelsPanel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MenuButtonPressed()
    {
        isMenuOpened = !isMenuOpened;
        animator.SetBool("isOpened", isMenuOpened); 
    }

    public void OnPressColorButton()
    {
        OpenSubMenu();
        ManageMenuPanels(true, false, false);
    }

    public void OnPressMaterialsButton()
    {
        OpenSubMenu();
        ManageMenuPanels(false, true, false);
    }

    public void OnPressWheelsButton()
    {
        OpenSubMenu();
        ManageMenuPanels(false, false, true);
    }

    private void ManageMenuPanels(bool color, bool material, bool wheel)
    {
        colorPannel.SetActive(color);
        materialPanel.SetActive(material);
        wheelsPanel.SetActive(wheel);
    }

    private void OpenSubMenu()
    {
        animator.SetTrigger("openSubMenu");
    }
}
