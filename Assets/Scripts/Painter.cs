﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Painter : MonoBehaviour {

    public Slider redSlider;
    public Slider greenSlider;
    public Slider blueSlider;

    public Material[] baseMaterial ;
    public Material[] baseDarkerMaterial;
    public Color color;
    public Renderer[] paintedObjects;
    public Renderer[] darkerPaintedObjects;

    // Use this for initialization
    void Start () {
        UpdateSliders();       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateSliders()
    {
        if (paintedObjects != null && paintedObjects.Length > 0)
        {
            Color defaultCarColor = paintedObjects[0].material.color;
            redSlider.value = defaultCarColor.r;
            greenSlider.value = defaultCarColor.g;
            blueSlider.value = defaultCarColor.b;
        }
    }

    public void RePaint()
    {
        float r, g, b;
        r = redSlider.value;
        g = greenSlider.value;
        b = blueSlider.value;
        Color newColor = new Color(r, g, b);

        RePaint(newColor);
    }

    public void RePaint(Color color)
    {
        foreach (Renderer renderer in paintedObjects)
        {
            renderer.material.color = color;
        }

        float h, s, v;
        Color.RGBToHSV(color, out h, out s, out v);
        Color darkerColor = Color.HSVToRGB(h, s, Mathf.Clamp01(v * 0.4f));
        foreach (Renderer renderer in darkerPaintedObjects)
        {
            renderer.material.color = darkerColor;
        }
    }

    public void ChangeMaterial(int materialIndex)
    {
        Color baseColor = paintedObjects[0].material.color;
        foreach (Renderer renderer in paintedObjects)
        {
            renderer.material = baseMaterial[materialIndex];
            renderer.material.color = baseColor;
        }

        baseColor = darkerPaintedObjects[0].material.color;
        foreach (Renderer renderer in darkerPaintedObjects)
        {
            renderer.material = baseMaterial[materialIndex];
            renderer.material.color = baseColor;
        }
    }
}
