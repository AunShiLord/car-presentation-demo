﻿using UnityEngine;
using UnityEditor;

public class SaveData
{
    public string name;
    public string slotName;
    public float r, g, b;
    public int materialIndex;
}