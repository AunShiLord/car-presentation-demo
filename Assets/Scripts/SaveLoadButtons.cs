﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadButtons : MonoBehaviour {

    public GameObject panel;
    public ConfigurationLoader loader;
    public Animator animator;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenSaveMenu()
    {
        if (panel.activeSelf == false)
        {
            animator.SetTrigger("OpenSaveMenu");
            loader.ChangeMode(true);
        }
        else
        {
            animator.SetTrigger("CloseMenu");
        }
        
        
    }

    public void OpenLoadMenu()
    {
        if (panel.activeSelf == false)
        {
            animator.SetTrigger("OpenLoadMenu");
            loader.ChangeMode(false);
        }
        else
        {
            animator.SetTrigger("CloseMenu");
        }        
    }
}
