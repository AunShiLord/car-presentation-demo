﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelButton : MonoBehaviour {

    public WheelsManager wheelManager;
    public GameObject topLeftWheel;
    public GameObject topRightWheel;
    public GameObject backLeftWheel;
    public GameObject backRightWheel;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetImage(Sprite sprite)
    {
        GetComponent<Image>().sprite = sprite;
    }

    public void SetWheels(GameObject tl, GameObject tr, GameObject bl, GameObject br)
    {
        topLeftWheel = tl;
        topRightWheel = tr;
        backLeftWheel = bl;
        backRightWheel = br;
    }

    public void SetNewWheels()
    {
        wheelManager.ChangeWheels(topLeftWheel, topRightWheel, backLeftWheel, backRightWheel);
    }
}
