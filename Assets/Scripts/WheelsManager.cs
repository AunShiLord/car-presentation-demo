﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelsManager : MonoBehaviour {

    public GameObject topLeftWheel;
    public GameObject topRightWheel;
    public GameObject backLeftWheel;
    public GameObject backRightWheel;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeWheels(GameObject topLeft, GameObject topRight, GameObject backLeft, GameObject backRight)
    {
        topLeft = GameObject.Instantiate(topLeft, gameObject.transform);
        topRight = GameObject.Instantiate(topRight, gameObject.transform);
        backLeft = GameObject.Instantiate(backLeft, gameObject.transform);
        backRight = GameObject.Instantiate(backRight, gameObject.transform);

        topLeft.transform.position = topLeftWheel.transform.position;
        topRight.transform.position = topRightWheel.transform.position;
        backLeft.transform.position = backLeftWheel.transform.position;
        backRight.transform.position = backRightWheel.transform.position;

        Destroy(topLeftWheel);
        Destroy(topRightWheel);
        Destroy(backLeftWheel);
        Destroy(backRightWheel);

        topLeftWheel = topLeft;
        topRightWheel = topRight;
        backLeftWheel = backLeft;
        backRightWheel = backRight;
    }
}
